export interface User {
    username?: string;
    password?: string;
    new_password?: string;
    email?:string;
    role?:any;
    role_id?:number;
    status?:number;
}
export interface Token {
    type?: string;
    token?: string;
    refreshToken?:string;
}