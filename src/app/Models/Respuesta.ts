import { Token, User } from "./User";
import { Categoria } from "./Categoria";
import { Vista } from "./Vista";
import { Role } from "./Role";

export interface Respuesta {
    mensaje?: string;
    usuario?: User;
    users?: User[];
    access_token?: Token;
    categories?: Categoria[];
    vistas?: Vista[];
    roles?: Role[];
}