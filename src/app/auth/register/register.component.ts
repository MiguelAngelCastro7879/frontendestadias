import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { User } from 'src/app/Models/User';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  public user :User ={}
  public load: boolean = true;
 
  public formGroup!: FormGroup;

  constructor(private _route: ActivatedRoute,private _router: Router,private _authService: AuthService,private _cookieService: CookieService, private formBuilder: FormBuilder) {
  }

  public ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(8), Validators.pattern("^@+[a-zA-Z0-9]*$")]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  get email() { return this.formGroup.get('email'); }

  get password() { return this.formGroup.get('password'); }

  get username() { return this.formGroup.get('username'); }

  public ngOnDestroy(): void {
  }

  public onSubmit(): void {
    this._authService.register(this.formGroup.value).subscribe({
      next:(accion)=>{this.load = false;},
      error:(error: Error)=>{this.load = true;},
      complete:()=>{this.load = true;},
    })
  }
}
