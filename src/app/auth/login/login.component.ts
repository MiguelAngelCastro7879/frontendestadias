import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/Models/User';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  public loginValid = true;
  public user: User = {}
  public load: boolean = true;
 
  public formGroup!: FormGroup;

  constructor( private _router: Router, private _authService: AuthService,  private formBuilder: FormBuilder) {
  }
  
  get email() { return this.formGroup.get('email'); }

  get password() { return this.formGroup.get('password'); }

  public ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  public ngOnDestroy(): void {
  }

  public onSubmit(): void {
    this.load = false;
    this._authService.login(this.formGroup.value).subscribe({
      next:(accion)=>{this.load = false;},
      error:(error: Error)=>{this.load = true;},
      complete:()=>{this.load = true;},
    })
  }
}