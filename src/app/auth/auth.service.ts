import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { catchError, tap, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Respuesta } from '../Models/Respuesta';
import { User } from '../Models/User';
import { errorMessage, successDialog } from '../shared/alerts';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private http: HttpClient, private _router: Router, private _cookieService: CookieService) {
  }
  
  login(info: User){
    return this.http.post<Respuesta>(`${environment.urlbase}/login`, info).pipe(
      tap((respuesta:Respuesta) =>{
        this.setToken(respuesta.access_token!.token!)
        this.setRefresh(respuesta.access_token!.refreshToken!)
        successDialog(respuesta.mensaje!)
        // alert()
        this._router.navigate(['/main']);
      }),
      catchError((error: any) => {
        errorMessage(error.error!.error!)
        return throwError( error );
      })
    )
  }
  
  register(info: User){
    return this.http.post<Respuesta>(`${environment.urlbase}/register`, info).pipe(
      tap((respuesta:Respuesta) =>{
        this.login(info).subscribe()
      })
    ).pipe(
      catchError((error: any) => {
        errorMessage(error.error!.error!)
        return throwError( error );
      })
    )
  }

  getUser(){
    return this.http.get<Respuesta>(`${environment.urlbase}/get/user`)
  }

  refrescarToken(){
    return this.http.post<Respuesta>(`${environment.urlbase}/refresh/token`, {refresh_token:this.getRefresh()}).pipe(
      tap((respuesta:Respuesta) =>{
        // console.log('token actualizado: ',respuesta.access_token!.token!)
        this.setToken(respuesta.access_token!.token!)
        
      }),
      catchError((error: any) => {
        this._router.navigate(['/auth/login']);
        return throwError( error );
      })
    )
  }

  cambiarContraseña(info: User){
    console.log("entra aqui")
    return this.http.post<Respuesta>(`${environment.urlbase}/change/password`,info).pipe(
      tap((respuesta:Respuesta) =>{
        console.log("entra aca")
        successDialog(respuesta.mensaje!)
      }),
      catchError((error: any) => {
        errorMessage(error.error!.error!)
        return throwError( error );
      })
    ) 
  }

  logout(){
    return this._cookieService.deleteAll()
  }

  setToken(token: string): void {
    this._cookieService.set('token',token,4,'/')
  }
  getToken() {
    return this._cookieService.get('token');
  }
  removeToken(): void {
    this._cookieService.delete('token');
  }
  isLoggedIn() {
    return !!this.getToken();
  }
  
  setRefresh(refresh: string): void {
    this._cookieService.set('refresh_token',refresh,4,'/')
  }

  getRefresh() {
    return this._cookieService.get('refresh_token');
  }

  removeRefresh(): void {
    this._cookieService.delete('refresh_token');
  }

}
