import { HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, catchError, filter, Observable, switchMap, take, throwError } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService {

  private isRefreshing = false;
  private refreshSubject: BehaviorSubject<any> = new BehaviorSubject<any>(this.isRefreshing)

  constructor(
    private router: Router,
    private _authService : AuthService,
    private _cookieService: CookieService
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  
    let request = req;

    if(!this._authService.getRefresh() && !this._authService.getToken()){
      console.log('empieza la redireccion')
      this.router.navigateByUrl('/auth/login')
    }
    // console.log('termina la redireccion')
    if (this._authService.getToken()) {
      request = this.addToken(req,this._authService.getToken())
    }

    return next.handle(request).pipe(
      catchError( (err:any) => {
        if (err.status === 401 && err instanceof HttpErrorResponse) {
          return this.manejador401(request, next)
        }if (err.status === 403 && err instanceof HttpErrorResponse) {
          console.log('te van a redireccionar')
          this._authService.removeToken()
          this.router.navigateByUrl('/auth/login')
          return throwError( err );
        }else{
          return throwError( err );
        }
      })
    );
  }
  addToken(request:HttpRequest<any>, token:any ){
    return request.clone({
      setHeaders: {
        authorization: `Bearer ${ token }`
      }
    });
  }
  manejador401(request: HttpRequest<any>, next: HttpHandler){
    // if(this._cookieService.get('refresh_token')){
      if(!this.isRefreshing){
        this.isRefreshing = true;
        this.refreshSubject.next(null)
        return this._authService.refrescarToken().pipe(
          switchMap((token:any)=>{
            this.isRefreshing = false;
            // console.log("nuevo token: ",token.access_token!.token!)
            this.refreshSubject.next(token.refreshToken)
            return next.handle(this.addToken(request, token.access_token!.token!))
          })
        )
      }
      return next.handle(request)
      // else{
      //   return this.refreshSubject.pipe(
      //     filter(token => token != null),
      //     take(1),
      //     switchMap(jwt=>{
      //       return next.handle(this.addToken(request, jwt))
      //     }),
      //   )
      // }
    // }
  }
}
