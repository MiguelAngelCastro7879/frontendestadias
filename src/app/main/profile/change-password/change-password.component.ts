import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/Models/User';
import { DialogData, ProfileComponent } from '../profile.component';

@Component({
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent {
  public formGroup!: FormGroup;
  public usuario: User = {};

  constructor(
    public dialogRef: MatDialogRef<ProfileComponent>,
    private formBuilder: FormBuilder,
    private _authService: AuthService
  ) {
    this.formGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  get password() { return this.formGroup.get('password'); }
  
  get email() { return this.formGroup.get('email'); }

  onSubmit(){
    this.usuario = this.formGroup.value
    // this._authService.cambiarContraseña(this.usuario).subscribe()
    // this.usuario
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}

