import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { tap } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/Models/User';
import { ChangePasswordComponent } from './change-password/change-password.component';


export interface DialogData {
  animal: string;
  name: string;
}


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: User = {}

  constructor(private _authService: AuthService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this._authService.getUser()
    .pipe(tap(respuesta=>{
      this.user! = respuesta.usuario!
    }))
    .subscribe()
  }
  
  openDialog(): void {
    const dialogRef = this.dialog.open(ChangePasswordComponent, {
      width: '400px',
    });
  }
}