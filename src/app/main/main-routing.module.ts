import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { ProfileComponent } from './profile/profile.component';
import { VistasComponent } from './pages/vistas/vistas.component';
import { UsersComponent } from './pages/users/users.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { AssignViewsComponent } from './pages/assign-views/assign-views.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path:'profile',
        component:ProfileComponent
      },
      {
        path: 'views',
        component: VistasComponent,  
      },
      {
        path: 'categories',
        component: CategoriesComponent,  
      },
      {
        path: 'users',
        component: UsersComponent,  
      },
      {
        path: 'assign/views',
        component: AssignViewsComponent,  
      }
    ]
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
