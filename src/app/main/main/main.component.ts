import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import { delay, filter } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
// import { User } from 'src/app/Models/User';
import { MenuService } from './menu.service';
import { Categoria } from 'src/app/Models/Categoria';
import { MatMenuPanel } from '@angular/material/menu';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  @Input('matMenuTriggerFor')
  menu!: MatMenuPanel<any>;
  

  panelOpenState = false;
  // user: User = {}
  categorias : Categoria[] = [] 
  suscripcion!: Subscription;
 
  constructor(private observer: BreakpointObserver, private router: Router, private _authService: AuthService,private _menuService: MenuService) {
  }

  ngOnInit(): void {
    this.getVistas()
    // this.suscripcion = this._menuService.refresh$
    // // .pipe(delay(300000))
    // .pipe(delay(30000))
    // .subscribe(()=>{
    //   // console.log("get vistas")
    //   this.getVistas()
    // })
  }
  
  ngOnDestroy(): void {
    this.suscripcion.unsubscribe()
  }

  ngAfterViewInit() {
    this.observer
      .observe(['(max-width: 800px)'])
      .pipe(delay(1))
      .subscribe((res:any) => {
        if (res.matches) {
          this.sidenav.mode = 'over';
          this.sidenav.close();
        } else {
          this.sidenav.mode = 'side';
          this.sidenav.open();
        }
      });

    this.router.events
      .pipe(
        filter((e) => e instanceof NavigationEnd)
      )
      .subscribe(() => {
        if (this.sidenav.mode === 'over') {
          this.sidenav.close();
        }
      });
  }

  // getUser(){
  //   this._authService.getUser().subscribe(respuesta=>{
  //     this.user = respuesta.usuario! 
  //   })
  // }

  getVistas(){
    
    this._menuService.getViews().subscribe(respuesta=>{
      this.categorias = respuesta.categories!
    }, error=>{
      console.log(error)
    })
  }

  logout(){
    this._authService.logout();
    setTimeout(()=>{
      this.router.navigate(['/auth/login']);
    }, 1000)
  }
}
