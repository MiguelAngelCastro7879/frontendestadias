import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, tap } from 'rxjs';
import { Respuesta } from 'src/app/Models/Respuesta';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private _refresh$ = new Subject<void>();
  constructor(private http: HttpClient) {}

  get refresh$(){
    return this._refresh$
  } 

  getViews(){
    return this.http.get<Respuesta>(`${environment.urlbase}/role/views`).pipe(
      tap(()=>{
        this._refresh$.next()
      })
    )
  }
}
