import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main/main.component';
import { SharedModule } from '../shared/shared.module'
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ProfileComponent } from './profile/profile.component';
import { VistasComponent } from './pages/vistas/vistas.component';
import { UsersComponent } from './pages/users/users.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { ChangePasswordComponent } from './profile/change-password/change-password.component';
import { FormsModule } from '@angular/forms';
import { AssignViewsComponent } from './pages/assign-views/assign-views.component';
import { CategoryDialogComponent } from './pages/categories/category-dialog/category-dialog.component';
import { ListViewsDialogComponent } from './pages/assign-views/list-views-dialog/list-views-dialog.component';
import { ViewDialogComponent } from './pages/vistas/view-dialog/view-dialog.component';
import { UserDialogComponent } from './pages/users/user-dialog/user-dialog.component';

@NgModule({
  declarations: [
    MainComponent,
    ProfileComponent,
    VistasComponent,
    UsersComponent,
    CategoriesComponent,
    ChangePasswordComponent,
    AssignViewsComponent,
    ViewDialogComponent,
    UserDialogComponent,
    CategoryDialogComponent,
    ListViewsDialogComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
    MatTreeModule,
    MatIconModule,
    MatButtonModule,
    FormsModule
  ],
})
export class MainModule { }
