import { Component, OnInit, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Vista } from 'src/app/Models/Vista';
import { successDialog } from 'src/app/shared/alerts';
import { ViewDialogComponent } from './view-dialog/view-dialog.component';
import { VistasService } from './vistas.service';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { connect } from 'rxjs';
import {EventSourcePolyfill} from 'ng-event-source';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-vistas',
  templateUrl: './vistas.component.html',
  styleUrls: ['./vistas.component.css']
})
export class VistasComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  
  displayedColumns: string[] = ['nombre', 'ruta', 'icono','categoria', 'status', 'opciones'];
  dataSource = new MatTableDataSource<Vista>();


  constructor(private _vistasService: VistasService, public dialog: MatDialog,private _authService: AuthService) {
    this.connect()
  }
  
  
  connect(): void {
    let source = new EventSourcePolyfill('http://192.168.1.5:3333/stream', {headers: { authorization: `Bearer ${this._authService.getToken()}`}});

    // let source = new EventSource('http://192.168.1.5:3333/stream')
      // withCredentials:true,
    // });

    source.addEventListener('message', message => {
      console.log(message)
        this.getVistas()
      
    });
  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.getVistas()
  }
  
  ngOnDestroy(): void {
  }
  
  toggleChange(id:number){
    this._vistasService.ActDesact(id).subscribe({
      next:(respuesta)=>{ 
        this.connect();
        successDialog(respuesta.mensaje!)
        this.getVistas()
        // console.log("se completo")
      }
    })
  }

  getVistas(){
    this._vistasService.getViews()
    .subscribe(respuesta=>{
      this.dataSource.data =  respuesta.vistas! 
    })
  }

  applyFilter(event:Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim();
  }
  
  
  openDialog(): void {
    const datos =  {
      function:"create"
    }
    const dialogRef = this.dialog.open(ViewDialogComponent, {
      width: '400px',
      data: datos
    });
    
    // dialogRef.afterClosed().subscribe(respuesta=>{
    //   if(respuesta)
    // })
  }
  
  actualizar(view: any) {
    const datos =  {
      function:"update",
      view: view
    }
    const dialogRef = this.dialog.open(ViewDialogComponent, {
      width: '400px',
      data: datos
    });
  }
  
  exportExcel() {
  
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('ProductData');
    
    worksheet.columns = [
      { header: 'Nombre', key: 'nombre'  },
      { header: 'Icono', key: 'icono' },
      { header: 'Ruta', key: 'ruta' },
      { header: 'Status', key: 'status' },
      { header: 'Nivel', key: 'nivel' },
      { header: 'Categoria', key: 'categoria' },
    ];
    this.dataSource.data.forEach(vista => {
      worksheet.addRow({
        nombre: vista.nombre!, 
        icono: vista.icono!, 
        ruta: vista.ruta!, 
        status: vista.status!,
        nivel: vista.nivel!, 
        categoria:vista.categoria!.nombre!
      },"n");
    });
    workbook.csv.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'text/csv' });
      fs.saveAs(blob, 'Categorias.csv');
    })
  }
}
