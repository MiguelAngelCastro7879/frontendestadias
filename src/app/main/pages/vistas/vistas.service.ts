import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, tap, throwError } from 'rxjs';
import { Respuesta } from 'src/app/Models/Respuesta';
import { Vista } from 'src/app/Models/Vista';
import { errorMessage, successDialog } from 'src/app/shared/alerts';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VistasService {

  constructor(private http: HttpClient) { 
  }

  getViews(){
    return this.http.get<Respuesta>(`${environment.urlbase}/get/views`)
  }
  
  postViews(info: Vista){
    return this.http.post<Respuesta>(`${environment.urlbase}/views`, info).pipe(
      tap((respuesta)=>{
        successDialog(respuesta.mensaje!)
      }),
      catchError((error: any) => {
        errorMessage(error.error!.error!)
        return throwError( error );
      })
    )
  }
  
  putViews(id:number ,info: Vista){
    return this.http.post<Respuesta>(`${environment.urlbase}/views/${id}`, info).pipe(
      tap((respuesta)=>{
        successDialog(respuesta.mensaje!)
      }),
      catchError((error: any) => {
        errorMessage(error.error!.error!)
        return throwError( error );
      })
    )
  }

  
  ActDesact(id_view:number){
    return this.http.delete<Respuesta>(`${environment.urlbase}/views/${id_view}`).pipe(
      catchError((error: any) => {
        errorMessage(error.error!.error!)
        return throwError( error );
      })
    )
  }
}
