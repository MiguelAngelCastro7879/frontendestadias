import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Categoria } from 'src/app/Models/Categoria';
import { Vista } from 'src/app/Models/Vista';
import { errorMessage } from 'src/app/shared/alerts';
import { CategoriesService } from '../../categories/categories.service';
import { VistasComponent } from '../vistas.component';
import { VistasService } from '../vistas.service';
import {EventSourcePolyfill} from 'ng-event-source';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-create-view',
  templateUrl: './view-dialog.component.html',
  styleUrls: ['./view-dialog.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewDialogComponent {

  public formGroup!: FormGroup;
  public vista: Vista = {};
  public categorias: Categoria[] = []

  constructor(public dialogRef: MatDialogRef<VistasComponent>,
    private formBuilder: FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _vistasService: VistasService,
    private _categoriaService: CategoriesService,
    private _authService: AuthService) {


    this.getCategorias()
    // console.log(data)
    if(data.function == "create"){
      this.formGroup = this.formBuilder.group({
        nombre: ['', [Validators.required, Validators.minLength(3)]],
        icono: ['', [Validators.required, Validators.minLength(3)]],
        nivel: ['', [Validators.required]],
        ruta: ['', [Validators.required, Validators.minLength(3)]],
        categoria_id: [null, [Validators.required]],
      });
    }
    if(data.function == "update"){
      this.formGroup = this.formBuilder.group({
        nombre: [data.view.nombre, [Validators.required, Validators.minLength(3)]],
        icono: [data.view.icono, [Validators.required, Validators.minLength(3)]],
        nivel: [data.view.nivel, [Validators.required]],
        ruta: [data.view.ruta, [Validators.required, Validators.minLength(3)]],
        categoria_id: [data.view.categoria.id, [Validators.required]],
      });
    }
  }
 async getCategorias() {
    await this._categoriaService.getCategories().subscribe({
      next:(respuesta)=>{
        this.categorias = respuesta.categories!
      },
    })
 }
  get nombre() { return this.formGroup.get('nombre'); }
  get icono() { return this.formGroup.get('icono'); }
  get nivel() { return this.formGroup.get('nivel'); }
  get ruta() { return this.formGroup.get('ruta'); }
  get categoria_id() { return this.formGroup.get('categoria_id'); }
  
  
  onSubmit(){
    this.vista = this.formGroup.value
    
    if(this.data.function == "create"){
      this._vistasService.postViews(this.vista).subscribe({
        complete:()=>{
          this.stream()
          // this.onNoClick()
          this.dialogRef.close(true);
        }
      })
    }
    if(this.data.function == "update"){
      this._vistasService.putViews(this.data.view.id,this.vista).subscribe({
        complete:()=>{
          this.stream()
          // this.onNoClick()
          this.dialogRef.close(true);
        }
      })
    }
  }
  onNoClick(): void {
    this.dialogRef.close(false);
  }
  
  
  stream(): void {
    let source = new EventSourcePolyfill('http://192.168.1.5:3333/stream', {headers: { authorization: `Bearer ${this._authService.getToken()}`}});

    // let source = new EventSource('http://192.168.1.5:3333/stream')
      // withCredentials:true,
    // });

    source.addEventListener('message', message => {
    });
  }
}
