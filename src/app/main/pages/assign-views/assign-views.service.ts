import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, tap, throwError } from 'rxjs';
import { Respuesta } from 'src/app/Models/Respuesta';
import { errorMessage, successDialog } from 'src/app/shared/alerts';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AssignViewsService {


  constructor(private http: HttpClient) { 
  }

  getRoles(){
    return this.http.get<Respuesta>(`${environment.urlbase}/roles`)
  }
  
  getCategoriesViews(){
    return this.http.get<Respuesta>(`${environment.urlbase}/category/views`)
  }

  getViewsByRole(id:number){
    return this.http.get<Respuesta>(`${environment.urlbase}/role/views/${id}`)
  }

  setViews(request:any){
    return this.http.post<Respuesta>(`${environment.urlbase}/set/views`, request).pipe(
      tap((respuesta:Respuesta) =>{
        successDialog(respuesta.mensaje!)
      }),
      catchError((error: any) => {
        errorMessage(error.error!.error!)
        return throwError( error );
      })
    )
  }
}
