import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Categoria } from 'src/app/Models/Categoria';
import { Vista } from 'src/app/Models/Vista';
import { CategoriesService } from '../../categories/categories.service';
import { AssignViewsComponent } from '../assign-views.component';
import { AssignViewsService } from '../assign-views.service';

@Component({
  selector: 'app-list-views-dialog',
  templateUrl: './list-views-dialog.component.html',
  styleUrls: ['./list-views-dialog.component.css']
})
export class ListViewsDialogComponent {

  public formGroup!: FormGroup;
  public categoriasTotales: Categoria[] = []
  public vistas: any[] = []
  public vistasSeleccionadas: any[] = []

  constructor(public dialogRef: MatDialogRef<AssignViewsComponent>, @Inject(MAT_DIALOG_DATA) public data: any,private _assignViewsService: AssignViewsService) {
    this._assignViewsService.getViewsByRole(data.role).subscribe({
      next:(respuesta)=>{
        respuesta.categories!.forEach(categoria => {
          categoria.vistas!.forEach(vista => {
            this.vistas.push(vista.id!)
          });
        });

        this._assignViewsService.getCategoriesViews().subscribe({
          next:(respuesta)=>{
            this.categoriasTotales = respuesta.categories!
            respuesta.categories!.forEach(categoria => {
              categoria.vistas!.forEach(vista => {
                if(this.vistas.includes(vista.id!)){
                  vista.selected=true
                  this.vistasSeleccionadas.push(vista)
                }else{
                  vista.selected=false
                  this.vistasSeleccionadas.push(vista)
                }
              });
            });
          }
        })
      }
    })
  }
  
  onSubmit(){
    this.vistas = []
    this.vistasSeleccionadas.forEach(vista => {
      if(vista.selected){
        this.vistas.push(vista.id)
      }
    });
    const request = {
      role:this.data.role,
      vistas:this.vistas
    }
    this._assignViewsService.setViews(request).subscribe({
      complete:()=>this.dialogRef.close()
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
