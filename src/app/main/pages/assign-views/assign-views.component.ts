import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Role } from 'src/app/Models/Role';
import { AssignViewsService } from './assign-views.service';
import { ListViewsDialogComponent } from './list-views-dialog/list-views-dialog.component';

@Component({
  selector: 'app-assign-views',
  templateUrl: './assign-views.component.html',
  styleUrls: ['./assign-views.component.css']
})
export class AssignViewsComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  
  displayedColumns: string[] = ['name',  'opciones'];
  dataSource = new MatTableDataSource<Role>();


  constructor(private _viewsService: AssignViewsService, public dialog: MatDialog) {
  }
  
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.getRoles()
  }
  
  // toggleChange(id:number){
  //   this._vistasService.ActDesact(id).subscribe({
  //     next:(respuesta)=>{ 
  //       successDialog(respuesta.mensaje!)
  //       this.getVistas()
  //       // console.log("se completo")
  //     }
  //   })
  // }

  getRoles(){
    this._viewsService.getRoles()
    .subscribe(respuesta=>{
      this.dataSource.data =  respuesta.roles! 
    })
  }

  applyFilter(event:Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim();
  }
  
  openDialog(id:number): void {
    const dialogRef = this.dialog.open(ListViewsDialogComponent, {
      width: '400px',
      data: {role: id},
    });
  }
  
  actualizar(id: number) {
    console.log("no esta enviando el id")
  }
}
