import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Categoria } from 'src/app/Models/Categoria';
import { successDialog } from 'src/app/shared/alerts';
import { CategoriesService } from './categories.service';
import { CategoryDialogComponent } from './category-dialog/category-dialog.component';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  
  displayedColumns: string[] = ['nombre', 'icono', 'status', 'opciones'];
  dataSource = new MatTableDataSource<Categoria>();


  constructor(private _categoriesService: CategoriesService, public dialog: MatDialog) {
  }
  
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.getCategories()
  }
  
  toggleChange(id:number){
    this._categoriesService.ActDesact(id).subscribe(respuesta=>{
      successDialog(respuesta.mensaje!)
      this.getCategories()
    })
  }

  getCategories(){
    this._categoriesService.getCategories()
    .subscribe(respuesta=>{
      // console.log(respuesta!)
      this.dataSource.data =  respuesta.categories! 
    })
  }

  applyFilter(event:Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim();
  }
  
  openDialog(): void {
    const datos =  {
      function:"create"
    }
    const dialogRef = this.dialog.open(CategoryDialogComponent, {
      width: '400px',
      data: datos
    });
  }
  
  actualizar(category: any) {
    const datos =  {
      function:"update",
      category: category
    }
    const dialogRef = this.dialog.open(CategoryDialogComponent, {
      width: '400px',
      data: datos
    });
    console.log(category)
  }
 
  exportExcel() {
  
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('ProductData');
    
    worksheet.columns = [
      { header: 'Nombre', key: 'nombre'  },
      { header: 'Icono', key: 'icono' },
      { header: 'Status', key: 'status' },
      { header: 'Nivel', key: 'nivel' },
    ];
    this.dataSource.data.forEach(categoria => {
      worksheet.addRow({nombre: categoria.nombre!, icono: categoria.icono!, status:categoria.status!, nivel:categoria.nivel!},"n");
    });
    workbook.csv.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'text/csv' });
      fs.saveAs(blob, 'Categorias.csv');
    })
  }
}
