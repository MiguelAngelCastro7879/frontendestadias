import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Categoria } from 'src/app/Models/Categoria';
import { CategoriesService } from '../categories.service';
import { CategoriesComponent } from '../categories.component';

@Component({
  selector: 'app-create-view',
  templateUrl: './category-dialog.component.html',
  styleUrls: ['./category-dialog.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryDialogComponent {

  public formGroup!: FormGroup;
  public categoria: Categoria = {};

  constructor(public dialogRef: MatDialogRef<CategoriesComponent>, @Inject(MAT_DIALOG_DATA) public data: any ,private formBuilder: FormBuilder,private _categoriesService: CategoriesService) {
    // console.log(data)
    if(data.function == "create"){
      this.formGroup = this.formBuilder.group({
        nombre: ['', [Validators.required, Validators.minLength(3)]],
        icono: ['', [Validators.required, Validators.minLength(3)]],
        nivel: ['', [Validators.required]],
      });
    }
    if(data.function == "update"){
      this.formGroup = this.formBuilder.group({
        nombre: [data.category.nombre, [Validators.required, Validators.minLength(3)]],
        icono: [data.category.icono, [Validators.required, Validators.minLength(3)]],
        nivel: [data.category.nivel, [Validators.required]],
      });
    }
  }

  get nombre() { return this.formGroup.get('nombre'); }
  get icono() { return this.formGroup.get('icono'); }
  get nivel() { return this.formGroup.get('nivel'); }
  
  onSubmit(){
    this.categoria = this.formGroup.value
    if(this.data.function == "create"){
      console.log(this.categoria)
      this._categoriesService.postCategory(this.categoria).subscribe({
        complete:()=>this.onNoClick()
      })
    }
    if(this.data.function == "update"){
      console.log(this.categoria)
      this._categoriesService.putCategory(this.data.category.id,this.categoria).subscribe({
        complete:()=>this.onNoClick()
      })
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
