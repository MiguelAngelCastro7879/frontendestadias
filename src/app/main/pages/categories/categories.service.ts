import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, tap, throwError } from 'rxjs';
import { Categoria } from 'src/app/Models/Categoria';
import { Respuesta } from 'src/app/Models/Respuesta';
import { errorMessage, successDialog } from 'src/app/shared/alerts';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { 
  }

  getCategories(){
    return this.http.get<Respuesta>(`${environment.urlbase}/categories`)
  }

  
  postCategory(info: Categoria){
    return this.http.post<Respuesta>(`${environment.urlbase}/categories`, info).pipe(
      tap((respuesta)=>{
        successDialog(respuesta.mensaje!)
      }),
      catchError((error: any) => {
        errorMessage(error.error!.error!)
        return throwError( error );
      })
    )
  }
  
  putCategory(id:number,info: Categoria){
    return this.http.put<Respuesta>(`${environment.urlbase}/categories/${id}`, info).pipe(
      tap((respuesta)=>{
        successDialog(respuesta.mensaje!)
      }),
      catchError((error: any) => {
        errorMessage(error.error!.error!)
        return throwError( error );
      })
    )
  }
  
  ActDesact(id_cat:number){
    return this.http.delete<Respuesta>(`${environment.urlbase}/categories/${id_cat}`)
  }
}
