import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSlideToggle, MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { delay, Subscription } from 'rxjs';
import { User } from 'src/app/Models/User';
import { UsersService } from './users.service';

import { Workbook } from 'exceljs';
import * as fs from 'file-saver';
import { MatDialog } from '@angular/material/dialog';
import { UserDialogComponent } from './user-dialog/user-dialog.component';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSlideToggle) toggle!: MatSlideToggle;

  displayedColumns: string[] = ['username', 'email', 'role','status', "opciones"];
  dataSource = new MatTableDataSource<User>();

  suscripcion!: Subscription;

  constructor(private _userService: UsersService, public dialog: MatDialog) {

  }

  ngOnInit(): void {
    this.getUsers()
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  toggleChange(id: number) {
    this._userService.ActDesact(id).subscribe(respuesta => {
      this.getUsers()
    })
  }

  getUsers() {
    this._userService.getUsers()
      .subscribe(respuesta => {
        this.dataSource.data = respuesta.users!
      })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim();
  }

  actualizar(user: any) {
    const datos =  {
      function:"update",
      user: user
    }
    const dialogRef = this.dialog.open(UserDialogComponent, {
      width: '400px',
      data: datos
    });
  }
  

  exportExcel() {
  
    let workbook = new Workbook();
    let worksheet = workbook.addWorksheet('ProductData');
    
    worksheet.columns = [
      { header: 'Username', key: 'username'  },
      { header: 'Email', key: 'email' },
      { header: 'Role', key: 'role' },
      { header: 'Status', key: 'status' },
    ];
    this.dataSource.data.forEach(user => {
      worksheet.addRow({username: user.username!, email: user.email!, role:user.role.name!, status:user.status!},"n");
    });
    workbook.csv.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'text/csv' });
      fs.saveAs(blob, 'Users.csv');
    })
  }
}
