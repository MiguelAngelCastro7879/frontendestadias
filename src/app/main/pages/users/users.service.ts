import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Respuesta } from 'src/app/Models/Respuesta';
import { environment } from 'src/environments/environment';
import { catchError, tap, throwError } from 'rxjs';
import { errorMessage, successDialog } from 'src/app/shared/alerts';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private _refresh$ = new Subject<void>();
  constructor(private http: HttpClient) {}

  get refresh$(){
    return this._refresh$
  }
  
  getUsers(){
    return this.http.get<Respuesta>(`${environment.urlbase}/get/users`)
  }

  
  putUsers(id:number,info:any){
    return this.http.put<Respuesta>(`${environment.urlbase}/users/${id}`, info).pipe(
      tap((respuesta:Respuesta) =>{
        successDialog(respuesta.mensaje!)
      }),
      catchError((error: any) => {
        errorMessage(error.error!.error!)
        return throwError( error );
      })
    ) 
  }
  
  getRoles(){
    return this.http.get<Respuesta>(`${environment.urlbase}/roles`)
  }
  
  ActDesact(id_user:number){
    return this.http.delete<Respuesta>(`${environment.urlbase}/users/${id_user}`).pipe(
      tap((respuesta:Respuesta) =>{
        successDialog(respuesta.mensaje!)
      }),
      catchError((error: any) => {
        errorMessage(error.error!.error!)
        return throwError( error );
      })
    )
  }
}
