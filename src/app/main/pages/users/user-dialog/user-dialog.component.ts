import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Categoria } from 'src/app/Models/Categoria';
import { Role } from 'src/app/Models/Role';
import { Vista } from 'src/app/Models/Vista';
import { errorMessage } from 'src/app/shared/alerts';
import { CategoriesService } from '../../categories/categories.service';
import { UsersComponent } from '../users.component';
import { UsersService } from '../users.service';
import { User } from 'src/app/Models/User';

@Component({
  selector: 'app-create-view',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserDialogComponent {

  public formGroup!: FormGroup;
  public user: User = {};
  public roles: Role[] = []
  public changePassword = false

  constructor(public dialogRef: MatDialogRef<UsersComponent>,private formBuilder: FormBuilder,@Inject(MAT_DIALOG_DATA) public data: any,private _userService: UsersService) {
    // console.log(data)
    this.getRoles()
    if(data.function == "create"){
      this.formGroup = this.formBuilder.group({
        username: ['', [Validators.required, Validators.minLength(8), Validators.pattern("^@+[a-zA-Z0-9]*$")]],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(8)]],
        role_id: [0, [Validators.required]],
        // changePassword: [false],
      });
    }
    if(data.function == "update"){
      this.formGroup = this.formBuilder.group({
        username: [data.user.username, [Validators.required, Validators.minLength(8), Validators.pattern("^@+[a-zA-Z0-9]*$")]],
        email: [data.user.email, [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(8)]],
        role_id: [data.user.role_id, [Validators.required]],
        // changePassword: [false],
      });
    }
  }

 async getRoles() {
    await this._userService.getRoles().subscribe({
      next:(respuesta)=>{
        this.roles = respuesta.roles!
      },
    })
 }
  get email() { return this.formGroup.get('email'); }

  // get changePassword() { return this.formGroup.get('changePassword'); }

  get password() { return this.formGroup.get('password'); }

  get username() { return this.formGroup.get('username'); }
  
  get role_id() { return this.formGroup.get('role_id'); }
  
  onSubmit(){
    if(this.changePassword){
      this.user = this.formGroup.value
    }else{
      this.user.username = this.username!.value
      this.user.email = this.email!.value
      this.user.role_id = this.role_id!.value
    }
    // console.log(this.user)
    // console.log(this.data.user.id)
    this._userService.putUsers(this.data.user.id, this.user).subscribe({
      complete:()=>this.onNoClick()
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
